<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderby('name')->paginate(10);
        return view('admin.category')->with(['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoryObject = new Category();       
        $categoryObject->name = $request['name']; 
        $categoryObject->status  = 0;  
        $categoryObject->save();
        return redirect('admin/categories')->withSuccess('Category Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = DB::table('categories')
                      ->where('id', '=',$id)
                      ->select('*')
                      ->get(); 
        return response()->json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required'
        ]);

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput();
        }
        else
        {
            $name = $request->input('name');
            $category = DB::table('categories')
                           ->where('name', '=', $name)
                           ->where('id', '!=', $id)
                           ->get();
            $valcount = $category->count();

            if ($valcount != 0) 
            {
                return redirect('admin/categories')->withError('Aldready Exists!');
            }
            else
            {
                $valupd = array(
                    'name' =>$request->input('name')
                );  
                Category::where('id',$id)->update($valupd);
                return redirect('admin/categories')->withSuccess('Category Updated Successfully!');
            }            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('admin/categories')->withSuccess('Category Deleted Successfully!');
    }
}
