<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Menu;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderby('name')->get();
        $menus = DB::table('menus')
            ->join('categories', 'menus.category_id', '=', 'categories.id')
            ->select('categories.name as category_name', 'menus.*')
            ->get();
        return view('admin.menu')->with(['categories' => $categories, 'menus' => $menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menuObject = new Menu();      
        $menuObject->category_id = $request->category;
        $menuObject->name = $request->name;
        $menuObject->price = $request->price;
        $menuObject->month_status = $request->month_status;
        $menuObject->active_status = $request->active_status;
        $menuObject->description = $request->description;
        
        $menuObject->image = time().'.'.$request['menu_image']->getClientOriginalExtension();
        $menu = time().'.'.$request['menu_image']->getClientOriginalExtension();
        $request['menu_image']->move(public_path('menu'),$menu);

        $menuObject->save();
        return redirect('admin/menu')->withSuccess('Menu Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = DB::table('menus')
                      ->where('id', '=',$id)
                      ->select('*')
                      ->get(); 
        return response()->json($menu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $request->category;
        $name = $request->name;
        $price = $request->price;
        $month_status = $request->month_status;
        $active_status = $request->active_status;
        $description = $request->description;

        if($request['image'] != '')
        {
            $image = time().'.'.$request['image']->getClientOriginalExtension();
            $menu = time().'.'.$request['logo']->getClientOriginalExtension();
            $request['image']->move(public_path('menu'),$menu);
            Menu::where('id', $id)->update(['image'=>$image]);
        }
      
        Menu::where('id', $id)->update(['category_id' => $category, 'name' => $name, 'price' => $price, 'month_status' => $month_status, 'active_status' => $active_status, 'description' => $description]);
        return redirect('admin/menu')->withSuccess('Menu Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();
        return redirect('admin/menu')->withSuccess('Menu Deleted Successfully!');
    }
}
