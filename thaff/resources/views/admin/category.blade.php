@extends('layouts.header')

@section('content')
<section class="content">
    <div class="container-fluid">
    	<div class="block-header">
            <h2>Categories</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	@if(Session::has('error'))
            <div class="alert bg-red alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('error') }}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert bg-green alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('success') }}
            </div>
            @endif
        	</div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                	<div class="header">
                	<h2>List Categories</h2>
                    </div>
                	<div class="body">
                		<div class="table-responsive">
                    		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
		                        <thead>
		                            <tr>
		                                <th>SlNo</th>
		                                <th>Category</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	@php
		                        		$i = 0;
		                        	@endphp
		                        	@foreach($categories as $category)   
		                        	@php
		                        		$i++;
		                        	@endphp
		                            <tr>
		                                <td>{{ $i }}</td>
		                                <td>{{ $category->name }}</td>
		                                <td>
		                                	<div class="demo-google-material-icon">
		                                		<a class="waves-effect m-r-20" data-toggle="modal" onclick="getCategories(this);" data-id="{{ $category->id }}" data-target="#editForm"><i class="material-icons">mode_edit</i></a>
		                                		<a id="del"class="waves-effect m-r-20" data-id="{{ $category->id }}"><i class="material-icons">delete</i></a>
		                                	</div>
		                                </td>
		                            </tr>
		                            @endforeach
		                        </tbody>
		                    </table>
                		</div>
            		</div>
            	</div>
            </div>
        </div>
        <div class="row clearfix js-sweetalert">
        	<button type="button" class="btn btn-primary waves-effect m-r-20 btn-circle-lg" data-toggle="modal" data-target="#addForm" style="float:right;"><div class="demo-google-material-icon"><i class="material-icons">add</i><span class="icon-name"></span></div></button>
        </div>
        <!-- Add form -->
        <div class="modal fade" id="addForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                	{{Form::open(array('id'=>'form_validation','method'=>'post','action'=>'Admin\CategoryController@store'))}}
                		{{ csrf_field() }}
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Add Category</h4>
	                    </div>
                    	<div class="modal-body">                       
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" required>
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button  class="btn btn-primary waves-effect" type="submit">SAVE</button>
	                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
	                    </div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- end add form-->
        <!-- Edit form -->
        <div class="modal fade" id="editForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                	<form id="edit_form_validation" method="post"  action="">
                		<input type="hidden" name="_method" value="put">
                   		{{ csrf_field() }}
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Edit Category</h4>
	                    </div>
                    	<div class="modal-body">                       
                            <div class="form-group form-float">
                            	<label>Name</label>
                                <div class="form-line">
                                    <input type="text" class="form-control" id="name" name="name" required>     
                                </div>
                            </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button  class="btn btn-primary waves-effect" type="submit">UPDATE</button>
	                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
	                    </div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- end edit form-->
    </div>
</section>
<!-- Jquery Core Js -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#del').click(function(){
		if (confirm('Are you sure?')) 
		{
			var id = $(this).attr('data-id');
			var token = '{{ csrf_token() }}';
			$.ajax({
				type: 'delete',
				url:"{{URL::to('admin/categories')}}"+'/'+id ,
				data:{id : id,_token :token },
				success : function(success){
				}
			});
			location.reload();
		}
		else
		{
			return false;
		}
	});
});

function getCategories(item)
{
	var id = $(item).data("id");  
    var base_url = "{{ URL::to('admin/categories') }}"; 
    $.ajax({
        url: base_url+'/'+id+'/edit',
        type:'GET',
        data: {id:id},
        success : function(response){
        	console.log(response);
          	$('#name').val(response[0]['name']);
           	$("#edit_form_validation").attr("action", "{{ URL::to('admin/categories') }}/" + response[0]['id']);
        }
    });
}
</script>
@endsection