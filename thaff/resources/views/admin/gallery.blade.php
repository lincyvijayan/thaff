@extends('layouts.header')

@section('content')
<section class="content">
    <div class="container-fluid">
    	<div class="block-header">
            <h2>Gallery</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	@if(Session::has('error'))
            <div class="alert bg-red alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('error') }}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert bg-green alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('success') }}
            </div>
            @endif
        	</div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                	<div class="header">
                	<h2>List Gallery</h2>
                    </div>
                	<div class="body">
                		<div class="table-responsive">
                    		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
		                        <thead>
		                            <tr>
		                                <th>SlNo</th>
                                        <th>Image</th>
                                        <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
		                        </tbody>
		                    </table>
                		</div>
            		</div>
            	</div>
            </div>
        </div>
        <div class="row clearfix js-sweetalert">
        	<button type="button" class="btn btn-primary waves-effect m-r-20 btn-circle-lg" data-toggle="modal" data-target="#addForm" style="float:right;"><div class="demo-google-material-icon"><i class="material-icons">add</i><span class="icon-name"></span></div></button>
        </div>
        <!-- Add form -->
        <div class="modal fade" id="addForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                	{{Form::open(array('id'=>'form_validation','method'=>'post','action'=>'Admin\GalleryController@store', 'enctype' => 'multipart/form-data'))}}
                		{{ csrf_field() }}
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Add Gallery Item</h4>
	                    </div>
                    	<div class="modal-body"> 
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Image</label>
                                    <div class="">
                                        <input type="file" class="form-control" name="menu_image" required>
                                    </div>
                                </div>
                            </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button  class="btn btn-primary waves-effect" type="submit">SAVE</button>
	                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
	                    </div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- end add form-->
    </div>
</section>
<!-- Jquery Core Js -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
function deleteMenu(item)
{
	if (confirm('Are you sure?')) 
	{
		var id = $(item).data("id");  
		var token = '{{ csrf_token() }}';
		$.ajax({
			type: 'delete',
			url:"{{URL::to('admin/menu')}}"+'/'+id ,
			data:{id : id,_token :token },
			success : function(success){
			}
		});
		location.reload();
	}
	else
	{
		return false;
	}
}

</script>
@endsection