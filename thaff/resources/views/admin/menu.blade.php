@extends('layouts.header')

@section('content')
<section class="content">
    <div class="container-fluid">
    	<div class="block-header">
            <h2>Menu</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	@if(Session::has('error'))
            <div class="alert bg-red alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('error') }}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert bg-green alert-dismissible">
            	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            	{{ Session::get('success') }}
            </div>
            @endif
        	</div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                	<div class="header">
                	<h2>List Menu</h2>
                    </div>
                	<div class="body">
                		<div class="table-responsive">
                    		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
		                        <thead>
		                            <tr>
		                                <th>SlNo</th>
		                                <th>Category</th>
		                                <th>Name</th>
                                        <th>Price</th>
                                        <th>Image</th>
                                        <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
                                    @php
                                        $no = 0;
                                    @endphp
                                    @foreach ($menus as $menu)
                                    @php
                                        $no++;
                                    @endphp
		                        	<tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $menu->category_name }}</td>
                                        <td>{{ $menu->name }}</td>
                                        <td>{{ $menu->price }}</td>
                                        <td><img width="40" height="40"  src="{{ asset('menu/'.$menu->image) }}"  alt="{{ $menu->image }}"></td>
                                        <td><div class="demo-google-material-icon">
                                                <a class="waves-effect m-r-20" data-toggle="modal" onclick="getMenus(this);" data-id="{{ $menu->id }}" data-target="#editForm"><i class="material-icons">mode_edit</i></a>
                                                <a onclick="deleteMenu(this)" class="waves-effect m-r-20" data-id="{{ $menu->id }}"><i class="material-icons">delete</i></a>
                                            </div></td>
                                    </tr>
                                    @endforeach
		                        </tbody>
		                    </table>
                		</div>
            		</div>
            	</div>
            </div>
        </div>
        <div class="row clearfix js-sweetalert">
        	<button type="button" class="btn btn-primary waves-effect m-r-20 btn-circle-lg" data-toggle="modal" data-target="#addForm" style="float:right;"><div class="demo-google-material-icon"><i class="material-icons">add</i><span class="icon-name"></span></div></button>
        </div>
        <!-- Add form -->
        <div class="modal fade" id="addForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                	{{Form::open(array('id'=>'form_validation','method'=>'post','action'=>'Admin\MenuController@store', 'enctype' => 'multipart/form-data'))}}
                		{{ csrf_field() }}
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Add Menu</h4>
	                    </div>
                    	<div class="modal-body">   
                            <div class="col-sm-6 selectClass">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Category</label>
                                    <div class="">
                                        <select class="form-control show-tick" name="category" required>
                                            <option value="">Select</option> 
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Price</label>
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="price" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Image</label>
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="menu_image" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <div class="demo-checkbox">                                
                                        <input type="checkbox" value="1" id="basic_checkbox_1" class="filled-in chk-col-light-blue" checked name="month_status"/>
                                        <label class="form-label" for="basic_checkbox_1">This Month Special</label>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <div class="demo-checkbox">                                
                                        <input type="checkbox" value="1" id="basic_checkbox_2" class="filled-in chk-col-light-blue" checked name="active_status" />
                                        <label class="form-label" for="basic_checkbox_2">Active</label>
                                    </div>
                                </div>
                            </div>                                                   
                            <div class="col-sm-12">   
                                <div class="form-group form-float">
                                    <label class="form-label">Description</label>
                                    <div class="form-line">
                                        <textarea class="form-control" name="description" required></textarea>
                                    </div>
                                </div>
                            </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button  class="btn btn-primary waves-effect" type="submit">SAVE</button>
	                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
	                    </div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- end add form-->
        <!-- Edit form -->
        <div class="modal fade" id="editForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                	<form id="edit_form_validation" method="post"  action="" enctype="multipart/form-data">
                		<input type="hidden" name="_method" value="put">
                   		{{ csrf_field() }}
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Edit Menu</h4>
	                    </div>
                    	<div class="modal-body">   
                            <div class="col-sm-6 selectClass">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Category</label>
                                    <div class="approve">
                                        <select class="form-control show-tick approve" name="category" required>
                                            <option value="">Select</option> 
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="name" name="name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Price</label>
                                    <div class="form-line">
                                        <input type="number" class="form-control" id="price" name="price" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <label class="form-label">Image</label>
                                    <div class="">
                                        <input type="file" class="form-control" id="menu_image" name="menu_image">
                                        <img width="40" height="40" id="imageDiv" src="">
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <div class="demo-checkbox">                                
                                        <input type="checkbox" value="1" id="basic_checkbox_3" class="filled-in chk-col-light-blue" checked name="month_status"/>
                                        <label class="form-label" for="basic_checkbox_3">This Month Special</label>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-sm-6">                    
                                <div class="form-group form-float">
                                    <div class="demo-checkbox">                                
                                        <input type="checkbox" value="1" id="basic_checkbox_4" class="filled-in chk-col-light-blue" checked name="active_status" />
                                        <label class="form-label" for="basic_checkbox_4">Active</label>
                                    </div>
                                </div>
                            </div>                                                   
                            <div class="col-sm-12">   
                                <div class="form-group form-float">
                                    <label class="form-label">Description</label>
                                    <div class="form-line">
                                        <textarea class="form-control" name="description" id="description" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
	                    <div class="modal-footer">
	                        <button  class="btn btn-primary waves-effect" type="submit">UPDATE</button>
	                        <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
	                    </div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- end edit form-->
    </div>
</section>
<!-- Jquery Core Js -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
function deleteMenu(item)
{
	if (confirm('Are you sure?')) 
	{
		var id = $(item).data("id");  
		var token = '{{ csrf_token() }}';
		$.ajax({
			type: 'delete',
			url:"{{URL::to('admin/menu')}}"+'/'+id ,
			data:{id : id,_token :token },
			success : function(success){
			}
		});
		location.reload();
	}
	else
	{
		return false;
	}
}

function getMenus(item)
{
	var id = $(item).data("id");  
    var base_url = "{{ URL::to('admin/menu') }}"; 
    $.ajax({
        url: base_url+'/'+id+'/edit',
        type:'GET',
        data: {id:id},
        success : function(response){
        	console.log(response);
          	$('#name').val(response[0]['name']);
            $('#price').val(response[0]['price']);
            $('#description').val(response[0]['description']);
            $('div.approve select').val(response[0]['category_id']);
            if (response[0]['month_status'] == 0)
                $('#basic_checkbox_3').prop('checked', false);
            else
                $('#basic_checkbox_3').prop('checked', true);
            if (response[0]['active_status'] == 0)
                $('#basic_checkbox_4').prop('checked', false);
            else
                $('#basic_checkbox_4').prop('checked', true);
            $('#imageDiv').attr('src', 'http://localhost:8000/menu/'+response[0]['image']);
           	$("#edit_form_validation").attr("action", "{{ URL::to('admin/menu') }}/" + response[0]['id']);
        }
    });
}
</script>
@endsection