@extends('layouts.loginHeader')

@section('content')
<div class="wrap-login100">
    <form role="form" id="form_validation" class="login100-form validate-form" method="POST" action="{{ route('login') }}" autocomplete="off"> 
        {{ csrf_field() }}
        <span class="login100-form-logo">
            <i class="zmdi zmdi-landscape"></i>
        </span>

        <span class="login100-form-title p-b-34 p-t-27">
            Log in
        </span>
        <span>{{ $errors->first('email') }}</span>
        <div class="wrap-input100 {{ $errors->has('email') ? ' has-error' : '' }}">
            <input class="input100" type="text" name="email" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
        </div>
        <span>{{ $errors->first('password') }}</span>
        <div class="wrap-input100 {{ $errors->has('password') ? ' has-error' : '' }}">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
        </div>        

        <div class="container-login100-form-btn">
            <button type="submit" class="login100-form-btn">
                Login
            </button>
        </div>
    </form>
</div>
@endsection
