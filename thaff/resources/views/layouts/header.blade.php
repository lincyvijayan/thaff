<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Thaff Delicacy</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="backend/image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        {!! Html::style('backend/plugins/bootstrap/css/bootstrap.css') !!}

        <!-- Waves Effect Css -->
        {!! Html::style('backend/plugins/node-waves/waves.css') !!}

        <!-- Animation Css -->
        {!! Html::style('backend/plugins/animate-css/animate.css') !!}

        <!-- Morris Chart Css-->
        {!! Html::style('backend/plugins/morrisjs/morris.css') !!}
        <!-- JQuery DataTable Css -->
        {!! Html::style('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') !!}
        <!-- Sweetalert Css -->
        {!! Html::style('backend/plugins/sweetalert/sweetalert.css') !!}
        <!-- Custom Css -->
        {!! Html::style('backend/css/style.css') !!}

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        {!! Html::style('backend/css/themes/all-themes.css') !!}
        
    </head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">Thaff Delicacy</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <!-- <img src="backend/images/user.png" width="48" height="48" alt="User" /> -->
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    @php
                        $path = Request::segment(1)."/".Request::segment(2);
                    @endphp
                    <li @if ($path == 'admin/home') class="active" @endif>
                        <a href="{{ url('admin/home') }}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li @if ($path == 'admin/categories') class="active" @endif>
                        <a href="{{ url('admin/categories') }}">
                            <i class="material-icons">text_fields</i>
                            <span>Categories</span>
                        </a>
                    </li>
                    <li @if ($path == 'admin/menu') class="active" @endif>
                        <a href="{{ url('admin/menu') }}">
                            <i class="material-icons">restaurant_menu</i>
                            <span>Menu</span>
                        </a>
                    </li>
                    <li @if ($path == 'admin/gallery') class="active" @endif>
                        <a href="{{ url('admin/gallery') }}">
                            <i class="material-icons">camera_enhance</i>
                            <span>Gallery</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    @yield('content')
    

    <!-- Jquery Core Js -->
    <script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('backend/plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <!-- Custom Js -->
    <script src="{{ asset('backend/js/admin.js') }}"></script>

    <script src="{{ asset('backend/js/pages/ui/modals.js') }}"></script>

    <script src="{{ asset('backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('backend/js/pages/forms/form-validation.js') }}"></script>

    <script src="{{ asset('backend/js/pages/index.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('backend/js/demo.js') }}"></script>
    <script src="{{ asset('backend/js/pages/forms/basic-form-elements.js') }}"></script>
</body>

</html>