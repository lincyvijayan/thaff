<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Thaff Delicacy</title>
        
		<link rel="icon" type="image/png" href="userLogin/images/icons/favicon.ico"/>
		{!! Html::style('userLogin/vendor/bootstrap/css/bootstrap.min.css') !!}
		{!! Html::style('userLogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css') !!}
		{!! Html::style('userLogin/fonts/iconic/css/material-design-iconic-font.min.css') !!}
		{!! Html::style('userLogin/vendor/animate/animate.css') !!}
		{!! Html::style('userLogin/vendor/css-hamburgers/hamburgers.min.css') !!}
		{!! Html::style('userLogin/vendor/animsition/css/animsition.min.css') !!}
		{!! Html::style('userLogin/vendor/select2/select2.min.css') !!}
		{!! Html::style('userLogin/vendor/daterangepicker/daterangepicker.css') !!}
		{!! Html::style('userLogin/css/util.css') !!}
		{!! Html::style('userLogin/css/main.css') !!}
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('userLogin/images/bg-01.jpg');">
			@yield('content')
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
	<script src="{{ asset('userLogin/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/animsition/js/animsition.min.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/select2/select2.min.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/daterangepicker/daterangepicker.js') }}"></script>
	<script src="{{ asset('userLogin/vendor/countdowntime/countdowntime.js') }}"></script>
	<script src="{{ asset('userLogin/js/main.js') }}"></script>
</body>
</html>