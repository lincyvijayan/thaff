<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Admin Routes
*/
Auth::routes();
Route::get('/admin', function () {
     return view('auth.login');
});

Route::get('/admin/home', 'Admin\HomeController@index');
Route::resource('/admin/categories', 'Admin\CategoryController');
Route::resource('/admin/menu', 'Admin\MenuController');
Route::resource('/admin/gallery', 'Admin\GalleryController');